/**
 * 
 */
package technion.boolpoly;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author eidanch
 * 
 */
public class BooleanVectorTest {

	/**
	 * Test method for {@link technion.boolpoly.BooleanVector#setBit(int)}.
	 */
	@Test
	public void testSetBit() {
		assertTrue(BooleanVector.zeroVector().setBit(3).test(3));
	}

	/**
	 * Test method for {@link technion.boolpoly.BooleanVector#oneCount()}.
	 */
	@Test
	public void testOneCount() {
		assertEquals(BooleanVector.createManually(1, 4, 5, 2).oneCount(), 4);
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.BooleanVector#or(technion.boolpoly.BooleanVector)}
	 * .
	 */
	@Test
	public void testOr() {
		assertEquals(BooleanVector.createManually(1, 4, 5, 2), BooleanVector
				.createManually(1, 4).or(BooleanVector.createManually(5, 2)));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.BooleanVector#permute(java.util.List)}.
	 */
	@Test
	public void testPermute() {
		final BooleanVector v1 = BooleanVector.createManually(1, 2, 4);
		final BooleanVector v2 = BooleanVector.createManually(1, 4, 6);
		final List<Integer> permutation = Arrays.asList(0, 4, 6, 5, 1, 3, 2);
		assertEquals(v1.permute(permutation), v2);
	}
}
