/**
 * 
 */
package technion.boolpoly;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import technion.boolpoly.collections.FixedDegreeVectorGenerator;

/**
 * @author Eidan
 * 
 */
public class BooleanPolynomialFinderTest {

	private final int testVariableCount = 5;

	private static boolean checkBooleanOnDimension(final Polynomial pol,
			final int degree) {
		for (final BooleanVector vector : FixedDegreeVectorGenerator.generate(
				degree, pol.variableCount()))
			if (!pol.isBooleanAssignment(vector))
				return false;
		return true;
	}

	/**
	 * Checks that the polynomial returned is boolean
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindBooleanity() {
		final Polynomial p = new BooleanPolynomialFinder()
				.findMinimum(testVariableCount);
		for (int d = 0; d <= 5; d++)
			assertTrue(checkBooleanOnDimension(p, d));
	}

	/**
	 * Checks that the polynomial returned is of minimal degree
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindMinimality() {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		booleanPolynomialFinder.findMinimum(testVariableCount);
		assertEquals(booleanPolynomialFinder.getBestDegree(), 3);
	}

	/**
	 * Checks that the all polynomials returned are boolean
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindAllBooleanity() {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		final List<Polynomial> ps = booleanPolynomialFinder
				.findAllMinimalPolynomials(testVariableCount);
		for (final Polynomial p : ps)
			for (int d = 0; d <= 5; d++)
				assertTrue(checkBooleanOnDimension(p, d));
	}

	/**
	 * Checks that the all polynomials returned are of minimal degree
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindAllMinimality() {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		final List<Polynomial> ps = booleanPolynomialFinder
				.findAllMinimalPolynomials(testVariableCount);
		assertEquals(booleanPolynomialFinder.getBestDegree(), 3);
		for (final Polynomial p : ps)
			assertEquals(3, p.degree());
	}

	/**
	 * Checks that the all polynomials returned are boolean
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindAllDimensionOrderBooleanity() {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		final List<Polynomial> ps = booleanPolynomialFinder
				.findAllNTBPDimensionOrder(testVariableCount, 3, true);
		for (final Polynomial p : ps)
			for (int d = 0; d <= p.variableCount(); d++)
				assertTrue(checkBooleanOnDimension(p, d));
	}

	/**
	 * Checks that the all polynomials returned are of minimal degree
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindAllDimensionOrderDegree() {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		final List<Polynomial> ps = booleanPolynomialFinder
				.findAllNTBPDimensionOrder(testVariableCount, 3, true);
		for (final Polynomial p : ps)
			assertTrue(p.degree() <= 3);
	}
}
