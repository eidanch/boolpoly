/**
 * 
 */
package technion.boolpoly;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author eidanch
 * 
 */
public class PolynomialTest {

	private void assertNonTrival(final Polynomial p) {
		assertTrue(0 == p.valueOf(BooleanVector.zeroVector()));
		for (int i = 0; i < p.variableCount(); i++)
			assertTrue(1 == p.valueOf(new BooleanVector(Arrays.asList(i))));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.Polynomial#isBooleanAssignment(technion.boolpoly.BooleanVector)}
	 * .
	 */
	@Test
	public void testIsBooleanAssignment() {
		assertTrue(Polynomial.nonTrivalBase(3).isBooleanAssignment(
				BooleanVector.createManually(1, 2, 3)));
	}

	/**
	 * Test method for {@link technion.boolpoly.Polynomial#nonTrivalBase(int)}.
	 */
	@Test
	public void testNonTrivalBase() {
		assertNonTrival(Polynomial.nonTrivalBase(7));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.Polynomial#isIsomorphic(technion.boolpoly.Polynomial)}
	 * .
	 */
	@Test
	public void testIsIsomorphic() {
		Polynomial p1 = new Polynomial(9);
		p1 = p1.addMonom(BooleanVector.createManually(1, 2, 3), 1);
		p1 = p1.addMonom(BooleanVector.createManually(1, 2, 4), 1);
		p1 = p1.addMonom(BooleanVector.createManually(4, 5, 7), -1);
		Polynomial p2 = new Polynomial(9);
		p2 = p2.addMonom(BooleanVector.createManually(1, 2, 3), 1);
		p2 = p2.addMonom(BooleanVector.createManually(1, 2, 4), 1);
		p2 = p2.addMonom(BooleanVector.createManually(3, 6, 8), -1);
		assertTrue(p1.isIsomorphic(p2));
	}

	@Test
	public void testPermute() {
		final Polynomial p1 = new Polynomial(9)
				.addMonom(new BooleanVector(Arrays.asList(1, 2, 3)), 1)
				.addMonom(new BooleanVector(Arrays.asList(1, 2, 4)), 1)
				.addMonom(new BooleanVector(Arrays.asList(4, 5, 7)), -1);
		final Polynomial p2 = new Polynomial(9)
				.addMonom(new BooleanVector(Arrays.asList(1, 2, 3)), 1)
				.addMonom(new BooleanVector(Arrays.asList(1, 2, 4)), 1)
				.addMonom(new BooleanVector(Arrays.asList(3, 6, 8)), -1);
		final List<Integer> permutation = Arrays.asList(0, 1, 2, 4, 3, 6, 5, 8,
				7);
		assertEquals(p1.permute(permutation), p2);
	}
}
