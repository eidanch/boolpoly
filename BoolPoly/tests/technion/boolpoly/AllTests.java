/**
 * 
 */
package technion.boolpoly;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import technion.boolpoly.collections.ExtendedSubsetGeneratorTest;
import technion.boolpoly.collections.FixedDegreeVectorGeneratorTest;
import technion.boolpoly.collections.MaskedFixedDegreeVectorGeneratorTest;
import technion.boolpoly.collections.PermutationGeneratorTest;
import technion.boolpoly.collections.StaticPermutationGeneratorTest;
import technion.boolpoly.collections.SubsetGeneratorTest;

/**
 * A suite of all tests
 * 
 * @author Eidan
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({ FixedDegreeVectorGeneratorTest.class,
		MaskedFixedDegreeVectorGeneratorTest.class, SubsetGeneratorTest.class,
		ExtendedSubsetGeneratorTest.class, BooleanPolynomialFinderTest.class,
		PermutationGeneratorTest.class, PolynomialTest.class,
		BooleanVectorTest.class, AllNTBPGeneratorTest.class,
		StaticPermutationGeneratorTest.class })
public class AllTests {

}
