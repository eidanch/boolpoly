/**
 * 
 */
package technion.boolpoly;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import technion.boolpoly.collections.FixedDegreeVectorGenerator;

/**
 * @author Eidan
 * 
 */
public class AllNTBPGeneratorTest {

	private final int variableCount = 5;
	private final int degree = 4;
	private final int nonIsomorphicCount = 70;

	@BeforeClass
	public static void beforeClassSetup() {
		BooleanPolynomialFinder.setCoefficientRestriction(true);
	}

	private static boolean checkBooleanOnDimension(final Polynomial pol,
			final int degree) {
		for (final BooleanVector vector : FixedDegreeVectorGenerator.generate(
				degree, pol.variableCount()))
			if (!pol.isBooleanAssignment(vector))
				return false;
		return true;
	}

	/**
	 * Checks that the all polynomials returned are boolean
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindAllBooleanity() {
		final List<Polynomial> ps = getResults();
		for (final Polynomial p : ps)
			for (int d = 0; d <= p.variableCount(); d++)
				assertTrue(checkBooleanOnDimension(p, d));
	}

	/**
	 * Checks that the all polynomials returned are of minimal degree
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindAllDegree() {
		final List<Polynomial> ps = getResults();
		for (final Polynomial p : ps)
			assertEquals(p.degree(), degree);
	}

	/**
	 * Checks that the all polynomials returned are of minimal degree
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindAllNonIsomorphicCount() {
		final List<Polynomial> ps = getResults();
		assertEquals(nonIsomorphicCount, ps.size());
	}

	/**
	 * @return
	 */
	private List<Polynomial> getResults() {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		final List<Polynomial> ps = booleanPolynomialFinder
				.findAllNTBPDimensionOrder(variableCount, degree, true);
		return ps;
	}

	/**
	 * Checks that the all polynomials returned are of minimal degree
	 * 
	 * Test method for
	 * {@link technion.boolpoly.BooleanPolynomialFinder#findMinimum(int)}.
	 */
	@Test
	public void testFindNone() {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		final List<Polynomial> ps = booleanPolynomialFinder
				.findAllNTBPDimensionOrder(7, 3, true);
		assertEquals(0, ps.size());
	}
}
