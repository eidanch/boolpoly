/**
 * 
 */
package technion.boolpoly.collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import technion.boolpoly.BooleanVector;

/**
 * @author Eidan
 * 
 */
public class MaskedFixedDegreeVectorGeneratorTest {

	private static final BooleanVector testMask = new BooleanVector(
			Arrays.asList(0, 2));

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testProperSize() {
		final List<BooleanVector> result = MaskedFixedDegreeVectorGenerator
				.generate(4, 8, testMask);
		assertEquals(result.size(), 15);
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsValidVector() {
		final List<BooleanVector> result = MaskedFixedDegreeVectorGenerator
				.generate(4, 8, testMask);
		final Collection<Integer> set = new HashSet<>();
		set.add(0);
		set.add(1);
		set.add(2);
		set.add(7);
		assertTrue(result.contains(new BooleanVector(set)));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsValidVector2() {
		final List<BooleanVector> result = MaskedFixedDegreeVectorGenerator
				.generate(4, 8, testMask);
		final Collection<Integer> set = new HashSet<>();
		set.add(0);
		set.add(2);
		set.add(4);
		set.add(5);
		assertTrue(result.contains(new BooleanVector(set)));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testNotContainsInvalid() {
		final List<BooleanVector> result = MaskedFixedDegreeVectorGenerator
				.generate(4, 8, testMask);
		final Collection<Integer> set = new HashSet<>();
		set.add(3);
		set.add(4);
		set.add(5);
		assertFalse(result.contains(new BooleanVector(set)));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testNotContainsInvalid2() {
		final List<BooleanVector> result = MaskedFixedDegreeVectorGenerator
				.generate(4, 8, testMask);
		final Collection<Integer> set = new HashSet<>();
		set.add(1);
		set.add(3);
		set.add(4);
		set.add(6);
		assertFalse(result.contains(new BooleanVector(set)));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testProperSizeLarge() {
		final List<BooleanVector> result = MaskedFixedDegreeVectorGenerator
				.generate(8, 14, testMask);
		assertEquals(result.size(), 924);
	}
}
