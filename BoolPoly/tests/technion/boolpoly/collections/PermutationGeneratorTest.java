/**
 * 
 */
package technion.boolpoly.collections;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * @author eidanch
 * 
 */
public class PermutationGeneratorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.PermutationGenerator#PermutationGenerator(int)}
	 * .
	 */
	@Test(timeout = 10)
	public void testPermutationGeneratorCount() {
		final Set<List<Integer>> ps = new HashSet<>();
		for (final List<Integer> p : new PermutationGenerator(4))
			ps.add(p);
		assertEquals(24, ps.size());
	}

	@Test(timeout = 100)
	public void testPermutationGenerator() {
		final Set<List<Integer>> ps = new HashSet<>();
		for (final List<Integer> p : new PermutationGenerator(3))
			ps.add(p);

		final Set<List<Integer>> expected = new HashSet<>();
		expected.add(Arrays.asList(0, 1, 2));
		expected.add(Arrays.asList(0, 2, 1));
		expected.add(Arrays.asList(1, 0, 2));
		expected.add(Arrays.asList(1, 2, 0));
		expected.add(Arrays.asList(2, 0, 1));
		expected.add(Arrays.asList(2, 1, 0));
		assertEquals(expected, ps);
	}

	@Test(timeout = 1000)
	public void testPermutationGeneratorCountLarge() {
		final Set<List<Integer>> ps = new HashSet<>();
		for (final List<Integer> p : new PermutationGenerator(8))
			ps.add(p);
		assertEquals(40320, ps.size());
	}
}
