/**
 * 
 */
package technion.boolpoly.collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import technion.boolpoly.Pair;

/**
 * @author Eidan
 * 
 */
public class ExtendedSubsetGeneratorTest {

	private Map<Integer, Pair<Integer, Integer>> map;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		map = new HashMap<>();
		map.put(1, new Pair<>(10, 11));
		map.put(2, new Pair<>(20, 21));
		map.put(3, new Pair<>(30, 31));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	private boolean mapEquals(final Map<Integer, Integer> a,
			final Map<Integer, Integer> b) {
		for (final Entry<Integer, Integer> e : a.entrySet())
			if (!(e.getValue().equals(b.get(e.getKey()))))
				return false;
		for (final Entry<Integer, Integer> e : b.entrySet())
			if (!(e.getValue().equals(a.get(e.getKey()))))
				return false;
		return true;
	}

	private boolean containsMap(final Iterable<Map<Integer, Integer>> i,
			final Map<Integer, Integer> c) {
		for (final Map<Integer, Integer> a : i)
			if (mapEquals(a, c))
				return true;
		return false;
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testProperSize() {
		final ArrayList<Map<Integer, Integer>> $ = new ArrayList<>();
		for (final Map<Integer, Integer> m : new ExtendedSubsetGenerator<>(map))
			$.add(m);
		assertTrue($.size() == 8);
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsFirstList() {
		final Map<Integer, Integer> a = new HashMap<>();
		a.put(1, 10);
		a.put(2, 20);
		a.put(3, 30);
		assertTrue(containsMap(new ExtendedSubsetGenerator<>(map), a));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsIntermediateList() {
		final Map<Integer, Integer> a = new HashMap<>();
		a.put(1, 11);
		a.put(2, 20);
		a.put(3, 31);
		assertTrue(containsMap(new ExtendedSubsetGenerator<>(map), a));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsLastList() {
		final Map<Integer, Integer> a = new HashMap<>();
		a.put(1, 11);
		a.put(2, 21);
		a.put(3, 31);
		assertTrue(containsMap(new ExtendedSubsetGenerator<>(map), a));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testProperSizeLarge() {
		final Map<Integer, Pair<Integer, Integer>> map2 = new HashMap<>();
		map2.put(1, new Pair<>(1, 2));
		map2.put(2, new Pair<>(1, 2));
		map2.put(3, new Pair<>(1, 2));
		map2.put(4, new Pair<>(1, 2));
		map2.put(5, new Pair<>(1, 2));
		map2.put(6, new Pair<>(1, 2));
		final ArrayList<Map<Integer, Integer>> $ = new ArrayList<>();
		for (final Map<Integer, Integer> m : new ExtendedSubsetGenerator<>(map2))
			$.add(m);
		assertEquals($.size(), 64);
	}
}
