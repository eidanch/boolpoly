/**
 * 
 */
package technion.boolpoly.collections;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Eidan
 * 
 */
public class SubsetGeneratorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	private boolean collectionEquals(final Collection<Integer> a,
			final Collection<Integer> b) {
		return a.containsAll(b) && b.containsAll(a);
	}

	private boolean containsCollection(final Iterable<List<Integer>> i,
			final Collection<Integer> c) {
		for (final Collection<Integer> a : i)
			if (collectionEquals(a, c))
				return true;
		return false;
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testProperSize() {
		final List<Integer> lst = new ArrayList<>();
		lst.add(1);
		lst.add(3);
		lst.add(5);
		final ArrayList<Collection<Integer>> $ = new ArrayList<>();
		for (final Collection<Integer> c : new SubsetGenerator<>(lst))
			$.add(c);
		assertTrue($.size() == 8);
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsIntermediateList() {
		final List<Integer> lst = new ArrayList<>();
		lst.add(1);
		lst.add(3);
		lst.add(5);
		final ArrayList<Integer> a = new ArrayList<>();
		a.add(1);
		a.add(3);
		assertTrue(containsCollection(new SubsetGenerator<>(lst), a));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsEmptyList() {
		final List<Integer> lst = new ArrayList<>();
		lst.add(1);
		lst.add(3);
		lst.add(5);
		final ArrayList<Integer> a = new ArrayList<>();
		assertTrue(containsCollection(new SubsetGenerator<>(lst), a));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testContainsFullList() {
		final List<Integer> lst = new ArrayList<>();
		lst.add(1);
		lst.add(3);
		lst.add(5);
		assertTrue(containsCollection(new SubsetGenerator<>(lst), lst));
	}

	/**
	 * Test method for
	 * {@link technion.boolpoly.collections.SubsetGenerator#execute()}.
	 */
	@Test
	public void testProperSizeLarge() {
		final List<Integer> lst = new ArrayList<>();
		lst.add(1);
		lst.add(2);
		lst.add(3);
		lst.add(4);
		lst.add(5);
		lst.add(6);
		final ArrayList<Collection<Integer>> $ = new ArrayList<>();
		for (final Collection<Integer> c : new SubsetGenerator<>(lst))
			$.add(c);
		assertTrue($.size() == 64);
	}
}
