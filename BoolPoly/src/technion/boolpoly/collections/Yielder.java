/**
 * 
 */
package technion.boolpoly.collections;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class allows dynamic construction of iterators
 * 
 * @author Eidan
 * 
 */
public abstract class Yielder<T> implements Iterable<T> {
	/**
	 * This flag is set only if the execute method has been executed
	 */
	private boolean executed = false;
	/**
	 * The result list
	 */
	private final ArrayList<T> results = new ArrayList<>();

	protected abstract void execute();

	/**
	 * Yields the given item from the iterator
	 * 
	 * @param e
	 *            The item to yield
	 */
	protected void yield(final T e) {
		results.add(e);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<T> iterator() {
		if (!executed) {
			execute();
			executed = true;
		}
		return results.iterator();
	}
}
