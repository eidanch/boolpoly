/**
 * 
 */
package technion.boolpoly.collections;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Represents a generator for subsets from a given list
 * 
 * @author Eidan
 * 
 */
public class SubsetGenerator<T> implements Iterable<List<T>> {
	/**
	 * The list to subset
	 */
	private final List<T> core;

	/**
	 * Creates a new instance of the class
	 * 
	 * @param core
	 *            The list to subset
	 */
	public SubsetGenerator(final List<T> core) {
		this.core = core;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<List<T>> iterator() {
		return new SubsetGeneratorIterator(0);
	}

	class SubsetGeneratorIterator implements Iterator<List<T>> {

		/**
		 * The index of the current subset
		 */
		private BigInteger current;

		/**
		 * Creates a new instance of the class
		 * 
		 * @param seed
		 *            The first subset index
		 */
		public SubsetGeneratorIterator(final int seed) {
			this.current = BigInteger.ZERO;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return current.bitLength() <= core.size();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#next()
		 */
		@Override
		public List<T> next() {
			BigInteger bitset = current;
			final List<T> $ = new ArrayList<>();
			for (int i = 0; bitset.compareTo(BigInteger.ZERO) > 0; i++) {
				if (bitset.and(BigInteger.ONE).compareTo(BigInteger.ONE) == 0)
					$.add(core.get(i));
				bitset = bitset.shiftRight(1);
			}
			current = current.add(BigInteger.ONE);
			return $;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new NotImplementedException();
		}

	}
}
