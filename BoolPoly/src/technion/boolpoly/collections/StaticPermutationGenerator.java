package technion.boolpoly.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class StaticPermutationGenerator {

	private static final HashMap<Integer, List<List<Integer>>> permutationCache = new HashMap<>();

	protected StaticPermutationGenerator() {
	}

	public static List<List<Integer>> generatePermutations(final int size) {
		if (permutationCache.containsKey(size))
			return Collections.unmodifiableList(permutationCache.get(size));
		final List<List<Integer>> $ = new ArrayList<>();
		final List<Integer> remaining = new ArrayList<>();
		for (int i = 0; i < size; i++)
			remaining.add(i);
		generatePermutations(new ArrayList<Integer>(), remaining, $);
		permutationCache.put(size, $);
		return $;
	}

	private static <T> void generatePermutations(final List<T> current,
			final List<T> remaining, final List<List<T>> results) {
		if (remaining.size() == 0) {
			results.add(new ArrayList<>(current));
			return;
		}
		for (final T t : remaining) {
			final List<T> nextCurrent = new ArrayList<T>(current);
			nextCurrent.add(t);
			final List<T> nextRemaining = new ArrayList<T>(remaining);
			nextRemaining.remove(t);
			generatePermutations(nextCurrent, nextRemaining, results);
		}
	}

}
