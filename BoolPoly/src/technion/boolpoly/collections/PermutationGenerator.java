package technion.boolpoly.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Generates all permutations using a lazy iterables
 * 
 * @author Eidan
 * 
 */
public class PermutationGenerator implements Iterable<List<Integer>> {

	private final int permutationSize;

	public PermutationGenerator(final int permutationSize) {
		this.permutationSize = permutationSize;
	}

	@Override
	public Iterator<List<Integer>> iterator() {
		return new PermutationIterator();
	}

	private class PermutationIterator implements Iterator<List<Integer>> {

		private boolean started;
		private final List<Integer> current;

		public PermutationIterator() {
			current = new ArrayList<Integer>();
			for (int i = 0; i < permutationSize; i++)
				current.add(i);
			started = false;
		}

		private int findLargestMonotone() {
			for (int i = current.size() - 2; i >= 0; i--)
				if (current.get(i) < current.get(i + 1))
					return i;
			return -1;
		}

		private int findLargestMonotoneSupporter(final int k) {
			for (int i = current.size() - 1; i > k; i--)
				if (current.get(k) < current.get(i))
					return i;
			throw new AssertionError();
		}

		private void reverseFrom(final int k) {
			for (int i = k; i < current.size() + k - i - 1; i++)
				Collections.swap(current, i, current.size() + k - 1 - i);
		}

		@Override
		public boolean hasNext() {
			return findLargestMonotone() != -1;
		}

		@Override
		public List<Integer> next() {
			if (!started) {
				started = true;
				return current;
			}
			final int k = findLargestMonotone();
			if (k == -1)
				throw new NoSuchElementException();
			final int l = findLargestMonotoneSupporter(k);
			Collections.swap(current, k, l);
			reverseFrom(k + 1);
			return current;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}
}
