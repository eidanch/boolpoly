/**
 * 
 */
package technion.boolpoly.collections;

import java.util.ArrayList;
import java.util.List;

import technion.boolpoly.BooleanVector;

/**
 * This class creates fixed degree vector lists
 * 
 * @author Eidan
 * 
 */
public final class FixedDegreeVectorGenerator {

	/**
	 * This class is static
	 */
	private FixedDegreeVectorGenerator() {
	}

	/**
	 * Generates a list of all fixed degree vectors of a given degree and size
	 * 
	 * @param degree
	 *            The number of 1s set in the result vectors
	 * @param variableCount
	 *            The length of the vectors
	 */
	public static List<BooleanVector> generate(int degree, int variableCount) {
		ArrayList<BooleanVector> $ = new ArrayList<>();
		generateRecursively(degree, variableCount, $,
				BooleanVector.zeroVector(), 0);
		return $;
	}

	/**
	 * Recursively generates all fixed degree vectors of a given degree and
	 * stores them in the result list
	 * 
	 * @param degree
	 *            The number of bits to add to the vector
	 * @param variableCount
	 *            The maximal index in the vector
	 * @param result
	 *            The result vector list
	 * @param vector
	 *            The base vector to build on
	 * @param nextIndex
	 *            The next index to set in the vector
	 */
	private static void generateRecursively(int degree, int variableCount,
			ArrayList<BooleanVector> result, BooleanVector vector, int nextIndex) {
		if (nextIndex + degree > variableCount)
			return;
		else if (degree == 0)
			result.add(vector);
		else {
			generateRecursively(degree - 1, variableCount, result,
					vector.setBit(nextIndex), nextIndex + 1);
			generateRecursively(degree, variableCount, result, vector,
					nextIndex + 1);
		}
	}

}