/**
 * 
 */
package technion.boolpoly.collections;

/**
 * Represents a map function
 * 
 * @author Eidan
 * 
 */
public interface Mapper<K, V> {
	/**
	 * Takes a single object of type K and maps it to a single object of type V
	 * 
	 * @param k
	 *            The object to map
	 * @return The map result
	 */
	V map(K k);
}
