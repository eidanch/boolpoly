/**
 * 
 */
package technion.boolpoly.collections;

import java.util.Iterator;

/**
 * This class provides lazy iteration over a list of lists
 * 
 * @author Eidan
 * 
 */
public class IterableIterable<T> implements Iterable<T> {

	/**
	 * The core iterable of iterables
	 */
	Iterable<Iterable<T>> core;

	/**
	 * Creates a new instance of the class
	 * 
	 * @param core
	 *            The iterable collection to be iterated
	 */
	public IterableIterable(Iterable<Iterable<T>> core) {
		this.core = core;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<T> iterator() {
		return new IterableIterator();
	}

	/**
	 * The iterator of the containing class
	 * 
	 * @author Eidan
	 * 
	 */
	class IterableIterator implements Iterator<T> {

		/**
		 * An iterator over the core
		 */
		private Iterator<Iterable<T>> coreIterator;
		/**
		 * A temporary list of results
		 */
		private Iterator<T> currentIterator;

		/**
		 * Creates a new instance of the class
		 */
		public IterableIterator() {
			coreIterator = core.iterator();
			if (coreIterator.hasNext())
				currentIterator = coreIterator.next().iterator();
			else
				currentIterator = null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return coreIterator.hasNext() || currentIterator.hasNext();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#next()
		 */
		@Override
		public T next() {
			if (!currentIterator.hasNext()) {
				currentIterator = coreIterator.next().iterator();
			}
			return currentIterator.next();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

}
