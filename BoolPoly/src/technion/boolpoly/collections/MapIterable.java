/**
 * 
 */
package technion.boolpoly.collections;

import java.util.Iterator;

/**
 * This class allows mapping iterables using custom mappers
 * @author Eidan
 *
 */
public class MapIterable<K,V> implements Iterable<V> {
	
	/**
	 * The core iterable to be mapped
	 */
	private final Iterable<K> core;
	/**
	 * The mapper function
	 */
	private final Mapper<K,V> mapper;
	
	/**
	 * Creates a new instance of the class
	 * @param core The core iterable to be mapped
	 * @param mapper The mapper function
	 */
	public MapIterable(Iterable<K> core, Mapper<K, V> mapper) {
		this.core = core;
		this.mapper = mapper;
	}


	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<V> iterator() {
		return new MapIterator();
	}
	
	/**
	 * Represents the iterator of the map
	 * @author Eidan
	 *
	 */
	class MapIterator implements Iterator<V> {
		
		/**
		 * Iterator over the core iterable
		 */
		private final Iterator<K> coreIterator;
		
		/**
		 * Creates a new instance of the class
		 */
		public MapIterator() {
			coreIterator = core.iterator();
		}

		/* (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return coreIterator.hasNext();
		}

		/* (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		@Override
		public V next() {
			return mapper.map(coreIterator.next());
		}

		/* (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
		
	}
}
