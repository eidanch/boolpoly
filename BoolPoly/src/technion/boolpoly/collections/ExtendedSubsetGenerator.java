/**
 * 
 */
package technion.boolpoly.collections;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import technion.boolpoly.Pair;

/**
 * Represents a generator for subsets from a given list
 * 
 * @author Eidan
 * 
 */
public class ExtendedSubsetGenerator<T1, T2> implements Iterable<Map<T1, T2>> {
	/**
	 * The list to subset
	 */
	private final Map<T1, Pair<T2, T2>> core;

	/**
	 * The value that denotes an item who should not be added to the set
	 */
	private final T2 ignoreValue;

	/**
	 * Creates a new instance of the class
	 * 
	 * @param core
	 *            The list to subset
	 */
	public ExtendedSubsetGenerator(final Map<T1, Pair<T2, T2>> core) {
		this(core, null);
	}

	/**
	 * Creates a new instance of the class
	 * 
	 * @param core
	 *            The list to subset
	 * @param ignoreValue
	 *            The value that denotes an item who should not be added to the
	 *            set
	 * 
	 */
	public ExtendedSubsetGenerator(final Map<T1, Pair<T2, T2>> core,
			final T2 ignoreValue) {
		this.core = core;
		this.ignoreValue = ignoreValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Map<T1, T2>> iterator() {
		return new SubsetGeneratorIterator();
	}

	class SubsetGeneratorIterator implements Iterator<Map<T1, T2>> {

		/**
		 * The index of the current subset
		 */
		private BigInteger current;

		private final List<T1> coreKeys;

		/**
		 * Creates a new instance of the class
		 * 
		 * @param seed
		 *            The first subset index
		 */
		public SubsetGeneratorIterator() {
			this.current = BigInteger.ZERO;
			this.coreKeys = new ArrayList<T1>(core.keySet());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return current.bitLength() <= core.size();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#next()
		 */
		@Override
		public Map<T1, T2> next() {
			BigInteger bitset = current;
			final Map<T1, T2> $ = new HashMap<>();
			for (int i = 0; i < core.size(); i++) {
				final T1 key = coreKeys.get(i);
				T2 value = null;
				if (bitset.and(BigInteger.ONE).compareTo(BigInteger.ONE) == 0)
					value = core.get(key).first;
				else
					value = core.get(key).second;
				if (ignoreValue == null || !value.equals(ignoreValue))
					$.put(key, value);
				bitset = bitset.shiftRight(1);
			}
			current = current.add(BigInteger.ONE);
			return $;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new NotImplementedException();
		}

	}
}
