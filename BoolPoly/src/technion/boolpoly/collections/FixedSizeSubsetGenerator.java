/**
 * 
 */
package technion.boolpoly.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class creates fixed size sets
 * 
 * @author Eidan
 * 
 */
public final class FixedSizeSubsetGenerator {

	/**
	 * This class is static
	 */
	private FixedSizeSubsetGenerator() {
	}

	/**
	 * Generates a list of all fixed size sets of a given size and over a given
	 * core
	 * 
	 * @param degree
	 *            The number of elements in each of the results sets
	 * @param core
	 *            The base set over which we build the subsets
	 */
	public static <T> List<Set<T>> generate(final int degree, final List<T> core) {
		final ArrayList<Set<T>> $ = new ArrayList<>();
		final Set<T> emptySet = Collections.emptySet();
		generateRecursively(degree, core, $, emptySet, 0);
		return $;
	}

	/**
	 * Recursively generates all fixed size sets of a given size and stores them
	 * in the result list
	 * 
	 * @param degree
	 *            The number of elements to add to the list
	 * @param core
	 *            The base collection
	 * @param result
	 *            The result set list
	 * @param current
	 *            The base set to build on
	 * @param nextIndex
	 *            The next element index to add to the set
	 */
	private static <T> void generateRecursively(final int degree,
			final List<T> core, final List<Set<T>> result,
			final Set<T> current, final int nextIndex) {
		if (nextIndex + degree > core.size())
			return;
		else if (degree == 0)
			result.add(current);
		else {
			final Set<T> current2 = new HashSet<T>(current);
			current2.add(core.get(nextIndex));
			generateRecursively(degree - 1, core, result, current,
					nextIndex + 1);
			generateRecursively(degree, core, result, current2, nextIndex + 1);
		}
	}

}