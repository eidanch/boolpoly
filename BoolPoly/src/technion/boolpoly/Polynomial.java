/**
 * 
 */
package technion.boolpoly;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import technion.boolpoly.collections.FixedDegreeVectorGenerator;
import technion.boolpoly.collections.StaticPermutationGenerator;

/**
 * Represents a multi-linear polynomial with integral coefficients
 * 
 * @author Eidan
 * 
 */
public class Polynomial implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5100353629461847464L;
	/**
	 * A map from monoms to their coefficients
	 */
	private final Map<BooleanVector, Integer> monoms;
	/**
	 * The number of variables in the polynomial
	 */
	private final int variableCount;
	/**
	 * The degree of the polynomial
	 */
	private Integer degree = null;

	/**
	 * Gets the polynomial's degree
	 * 
	 * @return The polynomial's degree
	 */
	public int degree() {
		if (degree != null)
			return degree;
		int $ = 0;
		for (final BooleanVector v : monoms.keySet())
			$ = Math.max($, v.oneCount());
		degree = $;
		return $;
	}

	/**
	 * Gets the number of variables in the polynomial
	 * 
	 * @return number of variables
	 */
	public int variableCount() {
		return variableCount;
	}

	/**
	 * Checks if a given assignment yields a boolean value in the polynomial
	 * 
	 * @param vector
	 *            The assignment
	 * @return True if the assignment is boolean, false otherwise
	 */
	public boolean isBooleanAssignment(final BooleanVector vector) {
		final Integer value = valueOf(vector);
		return value == 0 || value == 1;
	}

	/**
	 * Returns the value of the polynomial after assignment of a given vector
	 * 
	 * @param vector
	 *            The vector to assign
	 * @return The polynomial assignment value
	 */
	public Integer valueOf(final BooleanVector vector) {
		Integer $ = 0;
		for (final Map.Entry<BooleanVector, Integer> e : monoms.entrySet())
			$ += e.getValue() * e.getKey().assign(vector);
		return $;
	}

	/**
	 * Creates a new polynomial using the this polynomial as base, and adding a
	 * given monom
	 * 
	 * @param v
	 *            The monom to add
	 * @param coff
	 *            The monom's coefficient
	 * @return The new monom
	 */
	public Polynomial addMonom(final BooleanVector v, final Integer coff) {
		final Map<BooleanVector, Integer> m = new HashMap<>(monoms);
		// if (m.containsKey(v))
		// throw new RuntimeException();
		m.put(v, coff);
		final Polynomial $ = new Polynomial(m, variableCount);
		$.degree = Math.max(degree(), v.oneCount());
		return $;
	}

	/**
	 * Creates a new polynomial containing the this polynomials monoms, and the
	 * new monoms
	 * 
	 * @param newMonoms
	 *            A map of new monoms
	 * @return The new polynomial
	 */
	public Polynomial addMonoms(final Map<BooleanVector, Integer> newMonoms) {
		final Map<BooleanVector, Integer> m = new HashMap<>(monoms);
		// for (final BooleanVector v : newMonoms.keySet())
		// if (m.containsKey(v))
		// throw new RuntimeException();
		m.putAll(newMonoms);
		final Polynomial $ = new Polynomial(m, variableCount);
		// $.degree = Math.max(degree(), v.oneCount());
		return $;
	}

	/**
	 * Returns the coefficient of a given monom
	 * 
	 * @param v
	 *            The monom
	 * @return The coefficient
	 */
	public int getCoefficient(final BooleanVector v) {
		if (monoms.containsKey(v))
			return monoms.get(v);
		return 0;
	}

	/**
	 * Creates a new instance of the class
	 * 
	 * @param monoms
	 *            The monom-coefficient map
	 * @param variableCount
	 *            The number of variables
	 */
	public Polynomial(final Map<BooleanVector, Integer> monoms,
			final int variableCount) {
		this.monoms = monoms;
		this.variableCount = variableCount;
	}

	/**
	 * Creates a new instance of the class
	 * 
	 * @param variableCount
	 *            The number of variables
	 */
	public Polynomial(final int variableCount) {
		this(new HashMap<BooleanVector, Integer>(), variableCount);
	}

	/**
	 * Returns the base non trivial polynomial
	 * 
	 * @param variableCount
	 *            The number of variables in the polynomial
	 * @return The new polynomial
	 */
	public static Polynomial nonTrivalBase(final int variableCount) {
		final Polynomial $ = new Polynomial(
				new HashMap<BooleanVector, Integer>(), variableCount);
		for (final BooleanVector v : FixedDegreeVectorGenerator.generate(1,
				variableCount))
			$.monoms.put(v, 1);
		for (final BooleanVector v : FixedDegreeVectorGenerator.generate(2,
				variableCount))
			$.monoms.put(v, -1);
		return $;
	}

	@Override
	public String toString() {
		return "Polynomial:" + monoms.toString();
	}

	/**
	 * Creates a new polynomial which extends the base polynomial to be non
	 * trivial in the new dimension
	 * 
	 * @param base
	 *            The base polynomial
	 * @param newVariableCount
	 *            The new dimension
	 * @return The new polynomial
	 */
	public static Polynomial extendDimensionOfNonTrival(final Polynomial base,
			final int newVariableCount) {
		Polynomial $ = new Polynomial(base.monoms, newVariableCount);
		for (int i = base.variableCount(); i < newVariableCount; i++) {
			$ = $.addMonom(BooleanVector.createManually(i), 1);
			for (int j = 0; j < i; j++)
				$ = $.addMonom(BooleanVector.createManually(i, j), -1);
		}
		return $;
	}

	@Override
	public boolean equals(final Object obj) { // check if the hashcode is worth
												// it
		return (obj instanceof Polynomial) && this.hashCode() == obj.hashCode()
				&& monoms.equals(((Polynomial) obj).monoms);
	}

	Integer hashCode = null;

	@Override
	public int hashCode() {
		if (hashCode == null)
			hashCode = monoms.hashCode();
		return hashCode;
	}

	/**
	 * Checks if this polynomial is isomorphic to a given polynomial. Two
	 * polynomials are considered isomorphic if there exists a permutation of
	 * the variables that when applied to one polynomial, creates a polynomial
	 * equals to the other.
	 * 
	 * @param p
	 *            The polynomial to check
	 * @return True if the polynomials are isomorphic, false otherwise
	 */
	public boolean isIsomorphic(final Polynomial p) {
		if (!p.isomorphismSignature().equals(this.isomorphismSignature()))
			return false;
		for (final List<Integer> permutation : StaticPermutationGenerator
				.generatePermutations(variableCount()))
			if (this.permute(permutation).equals(p))
				return true;
		return false;
	}

	/**
	 * Checks if this polynomial is isomorphic to a polynimial in the given
	 * list.
	 * 
	 * Does not use signature optimization!
	 * 
	 * @param ps
	 *            The list of polynomials to check
	 * @return True if the polynomial is isomorphic to a polynomial on the list,
	 *         false otherwise
	 */
	public boolean isIsomorphic(final List<Polynomial> ps) {
		for (final List<Integer> permutation : StaticPermutationGenerator
				.generatePermutations(variableCount())) {
			final Polynomial currentPermuted = this.permute(permutation);
			for (final Polynomial p : ps)
				if (currentPermuted.equals(p))
					return true;
		}
		return false;
	}

	/**
	 * Creates a new polynomial, with variables permuted under a given
	 * permutation
	 * 
	 * @param permutation
	 *            The variable permutation
	 * @return The new polynomial
	 */
	public Polynomial permute(final List<Integer> permutation) {
		final Map<BooleanVector, Integer> newMonoms = new HashMap<>();
		for (final Entry<BooleanVector, Integer> entry : monoms.entrySet())
			newMonoms
					.put(entry.getKey().permute(permutation), entry.getValue());
		return new Polynomial(newMonoms, variableCount);
	}

	/**
	 * Returns a set of all polynomials isomorphic to this polynomial
	 * 
	 * @return A set of polynomials
	 */
	public Set<Polynomial> generateIsomorphisims() {
		final Set<Polynomial> $ = new HashSet<>();
		for (final List<Integer> permutation : StaticPermutationGenerator
				.generatePermutations(variableCount()))
			$.add(permute(permutation));
		return $;
	}

	private Map<Integer, Integer> isomorphismSignature = null;

	/**
	 * This method returns an object that must be equal for isomorphic
	 * polynomial
	 * 
	 * @return The isomorphism signature
	 */
	public Object isomorphismSignature() {
		// if (isomorphismSignature == null)
		isomorphismSignature = calculateIsomorphismSignature();
		return isomorphismSignature;
	}

	/**
	 * Calculates the signature.
	 * 
	 * The signature is composed of counts of boolean vector of each degrees as
	 * well as the sum of coefficients
	 * 
	 * @return The signature
	 */
	private Map<Integer, Integer> calculateIsomorphismSignature() {
		final Map<Integer, Integer> $ = new HashMap<Integer, Integer>();
		final Map<Integer, Integer> varCounts = new HashMap<Integer, Integer>();
		$.put(0, 0);
		for (final Entry<BooleanVector, Integer> e : monoms.entrySet()) {
			for (final Integer var : e.getKey())
				if (varCounts.containsKey(var))
					varCounts.put(var, varCounts.get(var) + 1);
				else
					varCounts.put(var, 1);

			final int oneCount = e.getKey().oneCount();
			if (oneCount <= 2)
				continue;
			if ($.containsKey(oneCount))
				$.put(oneCount, $.get(oneCount) + 1);
			else
				$.put(oneCount, 1);

			final int mOneCount = -oneCount;
			if ($.containsKey(mOneCount))
				$.put(mOneCount, $.get(mOneCount) + e.getValue());
			else
				$.put(mOneCount, e.getValue());

			$.put(0, $.get(0) + e.getValue());

		}

		final Map<Integer, Integer> varCountMult = new HashMap<Integer, Integer>();
		for (final Integer value : varCounts.values())
			if (varCountMult.containsKey(value))
				varCountMult.put(value, varCountMult.get(value) + 1);
			else
				varCountMult.put(value, 1);
		$.put(variableCount() + 1, varCountMult.hashCode());

		return $;
	}
}
