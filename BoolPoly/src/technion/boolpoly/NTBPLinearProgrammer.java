/**
 * 
 */
package technion.boolpoly;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import technion.boolpoly.collections.SubsetGenerator;

/**
 * This class creates linear programs of the .lp format corresponding to the
 * booleanity problems
 * 
 * @author Eidan
 * 
 */
public class NTBPLinearProgrammer {

	private static final String LP_HEADER_COMMENT = "/* A linear program to solve the n%dd%d NTBP problem */";
	private static final String LP_BOOLEANITY_CONSTRAINT_COMMENT = " // 0 <= p(%s) <= 1";
	private static final String LP_VARIABLE_CONSTRAINT_COMMENT = " // 0 <= x_%s <= 1";
	private static final String LP_OBJECTIVE = "min: ;";

	/**
	 * Write a program to an output stream
	 * 
	 * @param n
	 *            The number of variables
	 * @param d
	 *            The degree of the polynomial
	 * @param out
	 *            The output stream
	 */
	public void programToStream(final int n, final int d, final OutputStream out) {
		final PrintWriter pw = new PrintWriter(out);
		pw.println(String.format(LP_HEADER_COMMENT, n, d));

		final StringBuilder integerConstraint = new StringBuilder();
		integerConstraint.append("int ");

		pw.println(LP_OBJECTIVE);
		pw.println();
		final SubsetGenerator<Integer> s = new SubsetGenerator<Integer>(
				fullList(n));

		for (final List<Integer> l : s) {
			pw.println(getBooleanityConstraint(l, d));
			if (l.size() <= d && l.size() >= 3) {
				pw.println(getVariableConstraint(l));
				integerConstraint.append(var(l) + ",");
			}
		}
		integerConstraint.deleteCharAt(integerConstraint.lastIndexOf(","));
		integerConstraint.append(";");
		pw.println(integerConstraint.toString());
		pw.close();
	}

	/**
	 * Returns a string representing the booleanity constraint
	 * 
	 * @param x
	 *            The constrained variable
	 * @param d
	 *            The problem dimension
	 * @return A string representing the constraint
	 */
	private String getBooleanityConstraint(final List<Integer> x, final int d) {
		final SubsetGenerator<Integer> s = new SubsetGenerator<Integer>(
				new ArrayList<Integer>(x));
		final StringBuilder sb = new StringBuilder();
		int knownConstant = 0;
		for (final List<Integer> l : s) {
			if (l.size() > d || l.size() == 0)
				continue;
			if (l.size() == 1) {
				knownConstant += 1;
				continue;
			}
			if (l.size() == 2) {
				knownConstant -= 1;
				continue;
			}
			sb.append("+" + var(l) + " ");
		}
		final String sum = sb.toString();
		if (sum.length() == 0)
			return "";
		return "0 <= " + sum + " + " + knownConstant + " <= 1;"
				+ String.format(LP_BOOLEANITY_CONSTRAINT_COMMENT, x);
	}

	/**
	 * Returns a string representing the variable bounds
	 * 
	 * @param x
	 *            The variable to be bounded
	 * @return a string representing the constraint
	 */
	private String getVariableConstraint(final List<Integer> x) {
		return "-1 <= " + var(x) + "<= 1;"
				+ String.format(LP_VARIABLE_CONSTRAINT_COMMENT, x);
	}

	/**
	 * Returns a list of the numbers from 0 to n-1
	 * 
	 * @param n
	 *            a bound on the list
	 * @return A new list
	 */
	private List<Integer> fullList(final int n) {
		final ArrayList<Integer> $ = new ArrayList<>(n);
		for (int i = 0; i < n; i++)
			$.add(i);
		return $;
	}

	/**
	 * Returns a string representing the variable
	 * 
	 * @param x
	 *            variable as boolean vector
	 * @return string representing the variable
	 */
	private String var(final Iterable<Integer> x) {
		return String.format("x%d", varIndex(x));
	}

	/**
	 * Turns a boolean vector into a variable index
	 * 
	 * @param x
	 *            boolean vector
	 * @return variable index as integer
	 */
	private int varIndex(final Iterable<Integer> x) {
		int result = 0;
		for (final int i : x)
			result += Math.pow(2, i);
		return result;
	}
}
