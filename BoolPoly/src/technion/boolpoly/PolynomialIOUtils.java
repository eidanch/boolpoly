/**
 * 
 */
package technion.boolpoly;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Some utilities for importing and exporting polynomial data into/from files
 * 
 * @author Eidan
 * 
 */
public class PolynomialIOUtils {

	/**
	 * Creates a linear program in a .lp format
	 * 
	 * @throws FileNotFoundException
	 *             If the file was not found
	 * @throws IOException
	 *             If an error occured during write
	 */
	public static void createLinearProgramFile(final int n, final int d)
			throws FileNotFoundException, IOException {
		final NTBPLinearProgrammer lp = new NTBPLinearProgrammer();
		final FileOutputStream fos = new FileOutputStream(String.format(
				"ntbpn%dd%d.lp", n, d));
		lp.programToStream(n, d, fos);
		fos.flush();
		fos.close();
	}

	/**
	 * Writes a given list of polynomials to a given file
	 * 
	 * @param ps
	 *            A list of polynomial
	 * @param fileName
	 *            the name of the file to write
	 */
	public static void outputPolynomials(final List<Polynomial> ps,
			final String fileName) {
		FileOutputStream fos;
		ObjectOutputStream oos;
		try {
			fos = new FileOutputStream(fileName);
			oos = new ObjectOutputStream(fos);
			oos.writeInt(ps.size());
			for (final Polynomial p : ps)
				oos.writeObject(p);
			oos.close();
		} catch (final Exception e) {
			e.printStackTrace();
			System.out.println("Stream Error!");
			return;
		}
	}

	/**
	 * Writes a given list of polynomials to a given file in the csv format
	 * 
	 * @param ps
	 *            a list of polynomials
	 * @param dimension
	 *            the maximal dimension of the polynomials
	 * @param fileName
	 *            the file to write to
	 */
	public static void csvOutputPolynomials(final List<Polynomial> ps,
			final int dimension, final String fileName) {
		try {
			final PrintWriter pw = new PrintWriter(fileName);
			for (final Polynomial p : ps)
				pw.println(polynomialToCsv(p, dimension));
			pw.close();
		} catch (final Exception e) {
			e.printStackTrace();
			System.out.println("Output Error!");
		}
	}

	/**
	 * Returns a csv string representing a given polynomial
	 * 
	 * @param p
	 *            The polynomial
	 * @param dimension
	 *            The polynomial dimension
	 * @return A csv string
	 */
	public static String polynomialToCsv(final Polynomial p, final int dimension) {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Math.pow(2, dimension); i++) {
			sb.append(p.getCoefficient(BooleanVector.fromInteger(i)));
			if (i < Math.pow(2, dimension) - 1)
				sb.append(',');
		}
		return sb.toString();
	}

	/**
	 * Reads a list of polynomials from a file
	 * 
	 * @param fileName
	 *            The file to read
	 * @return A list of polynomials
	 */
	public static List<Polynomial> readPolynomialsFromFile(final String fileName) {
		FileInputStream fis;
		ObjectInputStream ois;
		final List<Polynomial> $ = new ArrayList<>();
		try {
			fis = new FileInputStream(fileName);
			ois = new ObjectInputStream(fis);
			final int count = ois.readInt();
			for (int i = 0; i < count; i++)
				$.add((Polynomial) ois.readObject());
			ois.close();
		} catch (final Exception e) {
			e.printStackTrace();
			System.out.println("Stream Error!");
			return null;
		}
		return $;
	}

}
