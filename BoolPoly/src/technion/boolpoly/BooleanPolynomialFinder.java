/**
 * 
 */
package technion.boolpoly;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import technion.boolpoly.collections.ExtendedSubsetGenerator;
import technion.boolpoly.collections.MapIterable;
import technion.boolpoly.collections.Mapper;
import technion.boolpoly.collections.MaskedFixedDegreeVectorGenerator;

/**
 * Finds the polynomial with the minimal degree for a given number of variables
 * 
 * @author Eidan
 * 
 */
public class BooleanPolynomialFinder {

	/**
	 * This flag should be on only if the coefficients of the monoms should be
	 * restricted to {-1,0,1}
	 */
	private static boolean coefficientRestriction = false;

	/**
	 * Sets the coefficient restriction flag for future runs
	 * 
	 * The restriction flag should be on only if the coefficients of the monoms
	 * should be restricted to {-1,0,1}
	 * 
	 * @param restrict
	 *            restriction flag
	 */
	public static void setCoefficientRestriction(final boolean restrict) {
		coefficientRestriction = restrict;
	}

	/**
	 * A global bound on the best degree
	 */
	private Integer bestDegree;

	/**
	 * Returns the minimal degree of the last run
	 * 
	 * @return
	 */
	public int getBestDegree() {
		return bestDegree;
	}

	/**
	 * Creates a new instance of the class
	 */
	public BooleanPolynomialFinder() {
		bestDegree = 0;
	}

	/**
	 * Finds and returns the minimal not trivial boolean polynomial
	 * 
	 * @param variableCount
	 *            The number of variables in the polynomial
	 * @return A Polynomial object representing the polynomial
	 */
	public Polynomial findMinimum(final int variableCount) {
		final Polynomial base = Polynomial.nonTrivalBase(variableCount);
		bestDegree = Integer.MAX_VALUE;
		return buildMinimal(base, 3);
	}

	/**
	 * Finds and returns the minimal not trivial boolean polynomial
	 * 
	 * @param variableCount
	 *            The number of variables in the polynomial
	 * @return A List of non trivial boolean polynomials
	 */
	public List<Polynomial> findAllMinimalPolynomials(final int variableCount) {
		final Polynomial base = Polynomial.nonTrivalBase(variableCount);
		findMinimum(variableCount);
		return buildAllMinimal(base, 3, BooleanVector.zeroVector());
	}

	/**
	 * Finds and returns the not trivial boolean polynomials, but builds the
	 * polynomials in ascending dimensions rather then in ascending degrees
	 * 
	 * @param variableCount
	 *            The number of variables in the polynomials
	 * @return A List of non trivial boolean polynomials
	 */
	public List<Polynomial> findAllNTBPDimensionOrder(final int variableCount,
			final int degree, final boolean exactDegree) {
		final List<Polynomial> bases = filterIsomorphisms(buildAll(
				Polynomial.nonTrivalBase(degree), 3, degree,
				BooleanVector.zeroVector()));
		final List<Polynomial> polys = buildDimensionOrder(bases,
				variableCount, degree);
		if (!exactDegree)
			return polys;
		final List<Polynomial> $ = new ArrayList<>();
		for (final Polynomial p : polys)
			if (p.variableCount() == variableCount && p.degree() == degree)
				$.add(p);
		return $;
	}

	public List<Polynomial> checkPolynomialExpendability(
			final List<Polynomial> ps, final int variableCount, final int degree) {
		final List<Polynomial> $ = new ArrayList<Polynomial>();
		for (int i = 0; i < ps.size(); i++) {
			final Polynomial p = ps.get(i);
			if (findNextDimension(p, degree, variableCount) != null) {
				System.out.println(i + ": Expandable!");
				$.add(p);
			} else
				System.out.println(i + ": Inxpandable!");
		}
		return $;
	}

	/**
	 * Builds a minimal degree boolean polynomial that contains a given base
	 * polynomial
	 * 
	 * @param base
	 *            A base to build the polynomial on, The polynomial should be
	 *            boolean relative to it's degree
	 * @param currDegree
	 *            The degree of the current polynomial
	 * @return The best polynomial found on the branch
	 */
	private Polynomial buildMinimal(final Polynomial base, final int currDegree) {
		Polynomial $ = null;
		if (currDegree >= bestDegree)
			return null;
		final BooleanVector mask = BooleanVector.zeroVector();
		for (final Polynomial pol : buildDegree(base, currDegree, mask)) {
			if (checkBooleanFromDimension(pol, currDegree + 1, mask)) {
				bestDegree = currDegree;
				return pol;
			}
			final Polynomial temp = buildMinimal(pol, currDegree + 1);
			if (temp != null && ($ == null || temp.degree() < $.degree()))
				$ = temp;
		}
		return $;
	}

	/**
	 * Builds all minimal degree boolean polynomials that contains a given base
	 * polynomial
	 * 
	 * @param base
	 *            A base to build the polynomial on, The polynomial should be
	 *            boolean relative to it's degree
	 * @param currDegree
	 *            The degree of the current polynomial
	 * @param mask
	 *            The a vector that limits the search space. All vectors in the
	 *            search space will contain the mask.
	 * @return The all polynomials of minimal degree which are boolean
	 */
	private List<Polynomial> buildAllMinimal(final Polynomial base,
			final int currDegree, final BooleanVector mask) {
		List<Polynomial> $ = new ArrayList<>();
		if (currDegree > bestDegree)
			return $;
		for (final Polynomial pol : buildDegree(base, currDegree, mask)) {
			if (checkBooleanFromDimension(pol, currDegree + 1, mask)) {
				bestDegree = currDegree;
				if ($.size() != 0 && currDegree < $.get(0).degree())
					$.clear();
				$.add(pol);
				continue;
			}
			final List<Polynomial> temp = buildAllMinimal(pol, currDegree + 1,
					mask);
			if (temp.size() != 0)
				if ($.size() == 0 || temp.get(0).degree() < $.get(0).degree())
					$ = temp;
				else if (temp.get(0).degree() == $.get(0).degree())
					$.addAll(temp);
		}
		return $;
	}

	/**
	 * Creates a list of all NTBP based on a given polynomial with a given
	 * degree. Build order is degree-ascending.
	 * 
	 * @param base
	 *            The base polynomial to build on
	 * @param targetDegree
	 *            The degree we wish to reach
	 * @param mask
	 *            A mask for the vectors to check
	 * @return The list of all built polynomials
	 */
	private static List<Polynomial> buildAll(final Polynomial base,
			final int startDegree, final int targetDegree,
			final BooleanVector mask) {
		final List<Polynomial> $ = new ArrayList<>();
		List<Polynomial> nextBases = new ArrayList<>();
		nextBases.add(base);
		for (int i = startDegree; i <= targetDegree; i++) {
			final List<Polynomial> currentBases = nextBases;
			nextBases = new ArrayList<>();
			for (final Polynomial b : currentBases) {
				if (checkBooleanFromDimension(b, i, BooleanVector.zeroVector()))
					$.add(b);
				for (final Polynomial p : buildDegree(b, i, mask))
					nextBases.add(p);
			}
		}
		for (final Polynomial p : nextBases)
			if (checkBooleanFromDimension(p, targetDegree + 1,
					BooleanVector.zeroVector()))
				$.add(p);
		return ($);
	}

	/**
	 * Finds the first NTBP based on a given polynomial
	 * 
	 * @param base
	 *            The base polynomial to build on
	 * @param degree
	 *            The degree of the base polynomial
	 * @param targetDegree
	 *            The degree we wish to reach
	 * @param mask
	 *            A mask for the vectors to check
	 * @return The list of all built polynomials
	 */
	private Polynomial findNTBP(final Polynomial base, final int degree,
			final int targetDegree, final BooleanVector mask) {
		if (checkBooleanFromDimension(base, 3, BooleanVector.zeroVector()))
			return base;
		if (degree > targetDegree)
			return null;
		for (final Polynomial p : buildDegree(base, degree, mask)) {
			final Polynomial $ = findNTBP(p, degree + 1, targetDegree, mask);
			if ($ != null)
				return $;
		}
		return null;
	}

	/**
	 * Returns a list of all NTBP over a base using dimension build order
	 * 
	 * Note: This function is an extreme performance bottle next. Must check
	 * Permutation generation function, and polynomial equality function
	 * 
	 * @param base
	 *            The base polynomial
	 * @param targetVariableCount
	 *            The target dimension
	 * @return The list of polynomials
	 */
	private static List<Polynomial> buildDimensionOrder(
			final List<Polynomial> bases, final int targetVariableCount,
			final int degree) {
		final List<Polynomial> $ = new ArrayList<>();
		$.addAll(bases);
		for (int i = bases.get(0).variableCount(); i < targetVariableCount; i++) {
			final List<Polynomial> current = filterIsomorphisms($);
			final List<Polynomial> next = new ArrayList<>();
			final int x = current.size();
			int j = 1;
			System.out.println("> Starting the build of dimension " + (i + 1));
			for (final Polynomial base : current) {
				System.out.println(">> Building over base no. " + j++
						+ " out of " + x + " at " + System.nanoTime());
				final List<Polynomial> nextDimensionPolynomials = buildNextDimension(
						base, degree, i + 1);
				System.out.println(">> Finished building over base no. " + j
						+ " out of " + x + ". Polynomial count: "
						+ nextDimensionPolynomials.size());
				next.addAll(nextDimensionPolynomials);
			}
			$.addAll(next);
		}
		System.out
				.println("Filtering isomorphisms... This might take a while...");
		return filterIsomorphisms($);
	}

	/**
	 * Builds all non NTBP that contain a specific base
	 * 
	 * @param base
	 *            The base polynomial
	 * @return All NTBP over base
	 */
	private static List<Polynomial> buildNextDimension(final Polynomial base,
			final int degree, final int dimension) {
		final BooleanVector mask = BooleanVector.createManually(dimension - 1);
		final List<Polynomial> $ = new ArrayList<>();
		final Polynomial extendedBase = Polynomial.extendDimensionOfNonTrival(
				base, dimension);
		for (final Polynomial pol : buildAll(extendedBase, 3, degree, mask))
			// TODO: a smarter filter, this seems rather wasteful
			$.add(pol);
		return filterIsomorphisms($);
	}

	/**
	 * Finds an NTBP over a given base
	 * 
	 * @param base
	 *            The base polynomial
	 * @return All NTBP over base
	 */
	private Polynomial findNextDimension(final Polynomial base,
			final int degree, final int dimension) {
		final BooleanVector mask = BooleanVector.createManually(dimension - 1);
		final Polynomial extendedBase = Polynomial.extendDimensionOfNonTrival(
				base, dimension);
		return findNTBP(extendedBase, 3, degree, mask);
	}

	/**
	 * Takes a list of polynomials and removes polynomials that have an
	 * isomorphic polynomial before them in the list
	 * 
	 * @param ps
	 *            A list of polynomials
	 */
	private static List<Polynomial> filterIsomorphisms(final List<Polynomial> ps) {
		if (ps.size() <= 1)
			return ps;
		final Map<Object, List<Polynomial>> signatureToPolynomials = new HashMap<>();
		final List<Polynomial> $ = new ArrayList<>();
		for (final Polynomial p : ps) {
			final Object sig = p.isomorphismSignature();
			if (!signatureToPolynomials.containsKey(sig))
				signatureToPolynomials.put(sig, new ArrayList<Polynomial>());
			signatureToPolynomials.get(sig).add(p);
		}
		for (final List<Polynomial> sigClass : signatureToPolynomials.values())
			$.addAll(primitiveFilterIsomorphisms(sigClass));
		// System.out.println("polys: " + ps.size());
		// System.out.println("keys: " +
		// signatureToPolynomials.keySet().size());
		// System.out.println("filtered: " + $.size());
		return $;
	}

	/**
	 * Takes a list of polynomials and removes polynomials that have an
	 * isomorphic polynomial before them in the list
	 * 
	 * @param ps
	 *            A list of polynomials
	 */
	private static List<Polynomial> primitiveFilterIsomorphisms(
			final List<Polynomial> ps) {
		if (ps.size() <= 1)
			return ps;
		final boolean[] removed = new boolean[ps.size()];
		final List<Polynomial> $ = new ArrayList<>();
		Arrays.fill(removed, false);
		for (int i = 0; i < ps.size() - 1; i++) {
			if (removed[i])
				continue;
			$.add(ps.get(i));
			final Collection<Polynomial> iso = ps.get(i)
					.generateIsomorphisims();
			for (int j = i + 1; j < ps.size(); j++)
				if (!removed[j] && iso.contains(ps.get(j)))
					removed[j] = true;
		}
		if (ps.size() > 0 && !removed[ps.size() - 1])
			$.add(ps.get(ps.size() - 1));
		return $;
	}

	/**
	 * Builds all polynomials on a given base and a given degree ToDo: Code not
	 * pretty, find a better way to write this
	 * 
	 * @param base
	 *            The base polynomial
	 * @param degree
	 *            The built degree
	 * @param mask
	 *            The a vector that limits the search space. All vectors in the
	 *            search space will contain the mask.
	 * @return An iterable of built polynomials
	 */
	private static Iterable<Polynomial> buildDegree(Polynomial base,
			final int degree, final BooleanVector mask) {
		final List<BooleanVector> monoms = vectorsOfDegree(degree,
				base.variableCount(), mask);

		for (final BooleanVector v : monoms)
			if (isNonBooleanityWitness(base, v))
				return Collections.emptyList();

		final Pair<Map<BooleanVector, Integer>, Map<BooleanVector, Pair<Integer, Integer>>> monomMapPair = seperateBooleanVectors(
				base, monoms);
		for (final Entry<BooleanVector, Integer> entry : monomMapPair.first
				.entrySet())
			base = base.addMonom(entry.getKey(), entry.getValue());
		final Polynomial newBase = base;
		final Mapper<Map<BooleanVector, Integer>, Polynomial> mapper = new Mapper<Map<BooleanVector, Integer>, Polynomial>() {
			@Override
			public Polynomial map(final Map<BooleanVector, Integer> k) {
				return newBase.addMonoms(k);
			}
		};
		final ExtendedSubsetGenerator<BooleanVector, Integer> extendedSubsetGenerator = new ExtendedSubsetGenerator<BooleanVector, Integer>(
				monomMapPair.second, 0);
		return new MapIterable<Map<BooleanVector, Integer>, Polynomial>(
				extendedSubsetGenerator, mapper);
	}

	/**
	 * Checks if all vectors of a given degree get a boolean result on the
	 * polynomial
	 * 
	 * @param pol
	 *            The checked polynomial
	 * @param degree
	 *            The checked degree
	 * @param mask
	 *            The a vector that limits the search space. All vectors in the
	 *            search space will contain the mask.
	 * @return True if the polynomial is boolean for vectors of given degree,
	 *         False otherwise
	 */
	private static boolean checkBooleanOnDimension(final Polynomial pol,
			final int degree, final BooleanVector mask) {
		for (final BooleanVector vector : vectorsOfDegree(degree,
				pol.variableCount(), mask))
			if (!pol.isBooleanAssignment(vector))
				return false;
		return true;
	}

	/**
	 * Checks if all vectors of a of degree greater then a given degree get a
	 * boolean result on the polynomial
	 * 
	 * @param pol
	 *            The checked polynomial
	 * @param degree
	 *            The checked degree
	 * @param mask
	 *            The a vector that limits the search space. All vectors in the
	 *            search space will contain the mask.
	 * @return True if the polynomial is boolean for vectors of degree greater
	 *         then a given degree, False otherwise
	 */
	private static boolean checkBooleanFromDimension(final Polynomial pol,
			final int degree, final BooleanVector mask) {
		for (int currentDegree = pol.variableCount(); currentDegree >= degree; currentDegree--)
			if (!checkBooleanOnDimension(pol, currentDegree, mask))
				return false;
		return true;
	}

	/**
	 * Returns a list of all vectors of a given degree
	 * 
	 * @param degree
	 *            The vector degree
	 * @param variableCount
	 *            The vector length
	 * @param mask
	 *            The a vector that limits the search space. All vectors in the
	 *            search space will contain the mask.
	 * @return The list of all result vectors
	 */
	private static List<BooleanVector> vectorsOfDegree(final int degree,
			final int variableCount, final BooleanVector mask) {
		return MaskedFixedDegreeVectorGenerator.generate(degree, variableCount,
				mask);
	}

	/**
	 * Checks if the base polynomial will be able to satisfy a given vector
	 * assuming only the vector degree monoms are addable
	 * 
	 * @param base
	 *            The base polynomial
	 * @param v
	 *            The vector to check
	 * @return True if the polynomial cannot be fully boolean, false otherwise
	 */
	private static Boolean isNonBooleanityWitness(final Polynomial base,
			final BooleanVector v) {
		final int value = base.valueOf(v);
		return value < -1 || value > 2;
	}

	/**
	 * Retrieves the possible coefficients of a monom in the context of a given
	 * base polynomial
	 * 
	 * @param base
	 *            The base polynomial
	 * @param v
	 *            The monom to check
	 * @return A list of integer coefficients
	 */

	private static List<Integer> possibleCoefficients(final Polynomial base,
			final BooleanVector v) {
		final int value = base.valueOf(v);
		final ArrayList<Integer> $ = new ArrayList<>();
		final List<Integer> range = possibleCoefficients(coefficientRestriction);
		if (range == null || range.contains(-value))
			$.add(-value);
		if (range == null || range.contains(1 - value))
			$.add(1 - value);
		return $;
	}

	/**
	 * A helper method that aggregates the boolean vectors in a way that allows
	 * efficient extension.
	 * 
	 * @param base
	 *            The base polynomial
	 * @param vectors
	 *            A list of vectors
	 * @return Two maps of vectors to possible coefficients
	 */
	private static Pair<Map<BooleanVector, Integer>, Map<BooleanVector, Pair<Integer, Integer>>> seperateBooleanVectors(
			final Polynomial base, final List<BooleanVector> vectors) {
		final Map<BooleanVector, Integer> $1 = new HashMap<>();
		final Map<BooleanVector, Pair<Integer, Integer>> $2 = new HashMap<>();
		for (final BooleanVector v : vectors) {
			final List<Integer> li = possibleCoefficients(base, v);
			if (li.size() == 1)
				$1.put(v, li.get(0));
			if (li.size() == 2)
				$2.put(v, new Pair<Integer, Integer>(li.get(0), li.get(1)));
		}
		return new Pair<>($1, $2);
	}

	/**
	 * Returns an array consisting of possible coefficients for monoms
	 * 
	 * @return The coefficient array, or null to denote unbounded range
	 */
	private static List<Integer> possibleCoefficients(final boolean bounded) {
		return bounded ? Arrays.asList(new Integer[] { -1, 0, 1 }) : null;
	}
}
