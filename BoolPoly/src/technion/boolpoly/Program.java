/**
 * Solved n=6 in few seconds
 * Solution for n=6 is {[0]=1, [1]=1, [0, 1]=-1, [2]=1, [0, 2]=-1, [3]=1, [0, 3]=-1, [1, 2]=-1, [1, 3]=-1, [4]=1, [0, 4]=-1, [0, 2, 3]=1, [2, 3]=-1, [1, 4]=-1, [5]=1, [0, 5]=-1, [0, 1, 4]=1, [1, 2, 3]=1, [2, 4]=-1, [1, 5]=-1, [0, 1, 5]=1, [0, 3, 4]=1, [3, 4]=-1, [2, 5]=-1, [0, 2, 5]=1, [1, 2, 4]=1, [3, 5]=-1, [1, 3, 5]=1, [4, 5]=-1, [2, 4, 5]=1, [3, 4, 5]=1}
 */
package technion.boolpoly;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Main program runner
 * 
 * @author Eidan
 * 
 */
public final class Program {

	/**
	 * Pattern for a result file name
	 */
	private static final String RESULT_FILENAME = "result_n%dd%d.obj";

	/**
	 * Run the main program
	 * 
	 * @param args
	 *            No arguments expected
	 * @throws IOException
	 */
	public static void main(final String[] args) throws IOException {
		int n = 6, d = 4;
		if (args.length == 2) {
			n = Integer.parseInt(args[0]);
			d = Integer.parseInt(args[1]);
		}
		System.out.println(String.format("Running with parameters n=%d, d=%d",
				n, d));
		final String outputFile = String.format(RESULT_FILENAME, n, d);
		final String csvOutputFile = String.format("ntbp_n%dd%d.csv", n, d);
		System.out.println("Running the NTBP builder...");
		runBuilder(n, d, outputFile);
		System.out.println("Builder done! object stored in " + outputFile
				+ ". Now writing csv...");
		outputPolynomialsAsCsv(n, outputFile, csvOutputFile);
		System.out.println("Csv done! csv writted to " + csvOutputFile + ".");

	}

	/**
	 * Transforms a file that was created using java serialization into a csv
	 * file representing the polynomials
	 */
	private static void outputPolynomialsAsCsv(final int n,
			final String inputObjFilename, final String outputCsvFilename) {
		final List<Polynomial> ps = PolynomialIOUtils
				.readPolynomialsFromFile(inputObjFilename);
		System.out.println("Polynomials read from file: " + ps.size());
		PolynomialIOUtils.csvOutputPolynomials(ps, ps.get(0).variableCount(),
				outputCsvFilename);
	}

	/**
	 * Prints the signature histogram for debugging purposes
	 * 
	 * @param ps
	 *            a list of polynomials
	 */
	@SuppressWarnings("unused")
	private static void printSignatureHistogram(final List<Polynomial> ps) {
		final Map<Object, List<Polynomial>> signatureToPolynomials = new HashMap<>();
		for (final Polynomial p : ps) {
			final Object sig = p.isomorphismSignature();
			if (!signatureToPolynomials.containsKey(sig))
				signatureToPolynomials.put(sig, new ArrayList<Polynomial>());
			signatureToPolynomials.get(sig).add(p);
		}
		System.out.println("polys: " + ps.size());
		System.out.println("keys: " + signatureToPolynomials.keySet().size());
		for (final Object sig : signatureToPolynomials.keySet())
			System.out.println(sig + ": "
					+ signatureToPolynomials.get(sig).size());
	}

	/**
	 * Runs the exaustive search builder
	 * 
	 * @param n
	 *            The target dimension
	 * @param d
	 *            The target degree
	 * @param fileName
	 *            The name of the output file
	 */
	public static void runBuilder(final int n, final int d,
			final String fileName) {
		final BooleanPolynomialFinder booleanPolynomialFinder = new BooleanPolynomialFinder();
		long nanotime = System.nanoTime();
		final List<Polynomial> ps = booleanPolynomialFinder
				.findAllNTBPDimensionOrder(n, d, true);
		final Set<Polynomial> pss = new HashSet<>(ps);
		nanotime = System.nanoTime() - nanotime;

		System.out.println("Solution Count: " + pss.size());
		System.out.println("Time in nanoseconds: " + nanotime);
		if (fileName != null)
			PolynomialIOUtils.outputPolynomials(ps, fileName);
	}

	static String ntbp_n9d4_m3 = "x1*x2*x4 + x1*x2*x5 + x1*x3*x4 + x1*x2*x6 + x1*x3*x5 + x2*x3*x4 + x1*x2*x7 + x1*x3*x6 + x1*x4*x5 + x2*x3*x5 + x1*x2*x8 + x1*x3*x7 + x1*x4*x6 + x2*x3*x6 + x2*x4*x5 + x1*x2*x9 + x1*x3*x8 + x1*x5*x6 + x2*x3*x7 + x2*x4*x6 + x3*x4*x5 + x1*x3*x9 + x2*x3*x8 + x2*x5*x6 + x3*x4*x6 + x2*x3*x9 + x3*x5*x6 + x1*x7*x8 + x4*x5*x7 + x1*x7*x9 + x2*x7*x8 + x4*x5*x8 + x4*x6*x7 + x1*x8*x9 + x2*x7*x9 + x3*x7*x8 + x4*x5*x9 + x4*x6*x8 + x5*x6*x7 + x2*x8*x9 + x3*x7*x9 + x4*x6*x9 + x4*x7*x8 + x5*x6*x8 + x3*x8*x9 + x4*x7*x9 + x5*x6*x9 + x5*x7*x8 + x4*x8*x9 + x5*x7*x9 + x6*x7*x8 + x5*x8*x9 + x6*x7*x9 + x6*x8*x9";
	static String ntbp_n9d4_m4 = "x1*x2*x4*x5 - x1*x2*x4*x6 - x1*x3*x4*x5 - x1*x2*x5*x6 - x1*x3*x4*x6 - x2*x3*x4*x5 - x1*x3*x5*x6 - x2*x3*x4*x6 - x2*x3*x5*x6 - x1*x2*x7*x8 - x1*x2*x7*x9 - x1*x3*x7*x8 - x1*x2*x8*x9 - x1*x3*x7*x9 - x2*x3*x7*x8 - x1*x3*x8*x9 - x2*x3*x7*x9 - x2*x3*x8*x9 - x4*x5*x7*x8 - x4*x5*x7*x9 - x4*x6*x7*x8 - x4*x5*x8*x9 - x4*x6*x7*x9 - x5*x6*x7*x8 - x4*x6*x8*x9 - x5*x6*x7*x9 - x5*x6*x8*x9";

	/**
	 * Creates the well known n9d4 ntbp
	 * 
	 * @return An ntbp of dimension 9 and degree 4
	 */
	public static Polynomial ntbp_n9d4() {
		Polynomial $ = new Polynomial(9);
		for (int i = 0; i < 9; i++)
			$ = $.addMonom(BooleanVector.createManually(i), 1);
		for (int i = 0; i < 9; i++)
			for (int j = i + 1; j < 9; j++)
				$ = $.addMonom(BooleanVector.createManually(i, j), -1);
		for (int i = 0; i < 9; i++)
			for (int j = i + 1; j < 9; j++)
				$ = $.addMonom(BooleanVector.createManually(i, j), -1);
		for (final String s : ntbp_n9d4_m3.split("\\+"))
			$ = $.addMonom(monomFromString(s), 1);
		for (final String s : ntbp_n9d4_m4.split("-"))
			$ = $.addMonom(monomFromString(s), -1);

		return $;
	}

	/**
	 * Creates a monom from a string
	 * 
	 * @param s
	 *            The string representing the monom
	 * @return A boolean vector representing the monom
	 */
	private static BooleanVector monomFromString(final String s) {
		BooleanVector $ = BooleanVector.zeroVector();
		for (final Character c : s.replaceAll("x", "").replace("*", "").trim()
				.toCharArray()) {
			final int i = Integer.parseInt(c.toString());
			$ = $.setBit(i - 1);
		}
		return $;
	}

}
