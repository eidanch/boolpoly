/**
 * 
 */
package technion.boolpoly;

/**
 * Represents an immutable pair of elements
 * 
 * @author Eidan
 * 
 */
public final class Pair<T1, T2> {
	/**
	 * The first element
	 */
	public final T1 first;
	/**
	 * The second element
	 */
	public final T2 second;

	/**
	 * Creates a new instance of the class
	 * 
	 * @param first
	 *            The first element
	 * @param second
	 *            The second element
	 */
	public Pair(final T1 first, final T2 second) {
		this.first = first;
		this.second = second;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{" + first + ", " + second + "}";
	}
}
