/**
 * 
 */
package technion.boolpoly;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Represents a boolean vector
 * 
 * @author Eidan
 * 
 */
public class BooleanVector implements Iterable<Integer>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5549134107777442243L;
	/**
	 * This set represents the bits set to 1
	 */
	private final SortedSet<Integer> setBits;

	/**
	 * Creates a new instance of the BooleanVector class
	 * 
	 * @param indieces
	 *            The indieces of the bits that are set to 1
	 */
	public BooleanVector(final Collection<Integer> indieces) {
		setBits = new TreeSet<>(indieces);
	}

	/**
	 * Tests if the bit at a given index is set to 1
	 * 
	 * @param index
	 *            The index to test
	 * @return True if the bit is set, false otherwise
	 */
	public boolean test(final int index) {
		return setBits.contains(index);
	}

	/**
	 * Tests if the bit at a given index is set to 1
	 * 
	 * @param index
	 *            The index to test
	 * @return 1 if the bit is set, 0 otherwise
	 */
	public int testInt(final int index) {
		return setBits.contains(index) ? 1 : 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof BooleanVector))
			return false;
		return setBits.equals(((BooleanVector) obj).setBits);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return setBits.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return setBits.toString();
	}

	/**
	 * Returns the next boolean vector in lexicographical order
	 * 
	 * @return The next boolean vector
	 */
	public BooleanVector next() {
		final Set<Integer> newSet = new HashSet<>(setBits);
		int i;
		for (i = setBits.first(); newSet.contains(i); i++)
			newSet.remove(i);
		newSet.add(i + 1);
		return new BooleanVector(newSet);
	}

	/**
	 * Gets a zero vector
	 * 
	 * @return a zero vector
	 */
	public static BooleanVector zeroVector() {
		return new BooleanVector(new HashSet<Integer>());
	}

	/**
	 * Assigns the given vector to this vector as if it were a monom
	 * 
	 * @param vector
	 *            The vector to assign
	 * @return 1 if the given vector contains this vector, 0 otherwise
	 */
	public Integer assign(final BooleanVector vector) {
		return vector.setBits.containsAll(this.setBits) ? 1 : 0;
	}

	/**
	 * Creates a new copy of this vector with a given bit set
	 * 
	 * @param index
	 *            The bit to set
	 * @return The new Vector
	 */
	public BooleanVector setBit(final int index) {
		final HashSet<Integer> newset = new HashSet<Integer>(setBits);
		newset.add(index);
		return new BooleanVector(newset);
	}

	/**
	 * Creates a vector from a set of indices to set
	 * 
	 * @param indieces
	 *            The indices to set
	 * @return The new vector
	 */
	public static BooleanVector createManually(final Integer... indices) {
		return new BooleanVector(Arrays.asList(indices));
	}

	/**
	 * Creates a new boolean vector from an integer representing a bit set
	 * 
	 * @param iVector
	 *            The integer bit set
	 * @return The new Vector
	 */
	public static BooleanVector fromInteger(int iVector) {
		final HashSet<Integer> newset = new HashSet<Integer>();
		for (int i = 0; iVector > 0; i++) {
			if (iVector % 2 == 1)
				newset.add(i);
			iVector /= 2;
		}
		return new BooleanVector(newset);
	}

	/**
	 * Returns the integer whose binary representation is a bit set of this
	 * vector
	 * 
	 * @return The integer
	 */
	public int toInteger() {
		int $ = 0;
		for (final Integer k : setBits)
			$ += (int) Math.pow(2, k);
		return $;
	}

	/**
	 * Gets the number of set bits in the vector
	 * 
	 * @return The number of set bits in the vector
	 */
	public int oneCount() {
		return setBits.size();
	}

	/**
	 * Returns the bitwise or of this vector and a given vector
	 * 
	 * @param v
	 *            The second vector to or
	 * @return The bitwise or of both vectors
	 */
	public BooleanVector or(final BooleanVector v) {
		final Set<Integer> union = new HashSet<Integer>(setBits);
		union.addAll(v.setBits);
		return new BooleanVector(union);
	}

	/**
	 * Applies the permutation to the represented set to create a new vector
	 * 
	 * @param permutation
	 *            The permutation
	 * @return The new vector
	 */
	public BooleanVector permute(final List<Integer> permutation) {
		final Set<Integer> newSet = new HashSet<>();
		for (final Integer i : setBits)
			newSet.add(permutation.get(i));
		return new BooleanVector(newSet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Integer> iterator() {
		return setBits.iterator();
	}

	/**
	 * @param variableCount
	 * @return
	 */
	public static BooleanVector onesVector(final int variableCount) {
		final List<Integer> set = new ArrayList<Integer>();
		for (int i = 0; i < variableCount; i++)
			set.add(i);
		return new BooleanVector(set);
	}
}
